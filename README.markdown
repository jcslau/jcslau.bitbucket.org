# Connect Training Exercise 1

## Prerequisites

Follow the setup on the [Connect Docs][1] to get a cloud instance and your local dev setup.

## Create a Bitbucket repo

** NB. If your Bitbucket username contains an "_" please create a new Bitbucket account with alphanumeric characters only**

In Bitbucket, fork this repository and give it the name: 
    
    <Bitbucket username>.bitbucket.org

Your new repository will then be served on the web at
    
    https://<Bitbucket username>.bitbucket.io

and can be used to host your static addon!

You can see more info on the [Publishing a Website on Bitbucket Cloud][2] page
    
## Create the example addon

  1. Update the atlassian-connect.json descriptor "baseUrl" field to point to your online repo (and not mine) 

If it is working then you should be able to navigate to
    
    https://<Bitbucket username>.bitbucket.io/atlassian-connect.json

and see your Atlassian Connect Descriptor.

## Installing the add-on, onto the instance

On your cloud JIRA instance, navigate to the "Manage add-ons" Page in the admin space. To do so the
fast way:

 1. Navigate to JIRA and click the '.' key on your keyboard.
 1. Type in 'Manage add-ons' and select that option from the dropdown.

This will take you to the "Manage add-ons" page.

Once you are on the Manage add-ons page, click the text labelled "Upload add-on", then paste in your 
working discriptor link from above

    https://<Bitbucket username>.bitbucket.io/atlassian-connect.json

After pressing "Upload" in the dialog, JIRA will attempt to dowload the descriptor, and install the add-on for use straight away. 
(*Important:* You will need to refresh the page to see the effects of the reinstall)

There should now be a button in the top nav bar called 'Click me', clicking on this will load the "hello-you.html" document inside a JIRA General Page.

### Doing something useful

This section of the tutorial will be mainly focussed on the "hello-you.html" document.

Within the JavaScript segment of this page, we are downloading a script called "all.js" from the product instance.
    
    $.getScript(allJS, function () {
    ...

This script allows you to interact with the host product (JIRA) through the users web browser.

Just under this, is a block of code commented out. This uses the 'request' method to query for the current users name. Uncomment this code, and commit the file.

This code will automatically be pushed to you browser on the next page refresh.
Note that if it didn't change anything, your local Chrome browser maybe caching the old version, and a hard refresh (cmd + shift + r) or opening dev tools (cmd + option + i) and turning on "Disable Caching" in the network panel will be required

So, what does the script do? And what changed on the page?

### Extra credit

If there's still time, try and add the same panel to the [Right-side of the 'View Issue' page location][3]

You will need to modify the atlassian-connect.json descriptor and re-install your add-on ...

## Further documentation

* [JavaScript API](https://developer.atlassian.com/static/connect/docs/latest/concepts/javascript-api.html)
* [Scopes Documentation](https://developer.atlassian.com/static/connect/docs/latest/scopes/scopes.html)
* [REST API Browser](https://jira.atlassian.com/plugins/servlet/restbrowser)

 [1]: https://developer.atlassian.com/static/connect/docs/latest/guides/development-setup.html
 [2]: https://confluence.atlassian.com/display/BITBUCKET/Publishing+a+Website+on+Bitbucket+Cloud
 [3]: https://developer.atlassian.com/jiracloud/jira-platform-modules-view-issue-page-39988380.html
